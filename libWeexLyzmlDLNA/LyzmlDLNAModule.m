//
//  LyzmlDLNAModule.m
//  libWeexLyzmlDLNA
//
//  Created by edz on 2020/3/31.
//  Copyright © 2020 coolanimals. All rights reserved.
//

#import "LyzmlDLNAModule.h"
//#import "WXUtility.h"
#import "DCUniDefine.h"
#import "CLUPnPServer.h"
#import "CLUPnPRenderer.h"
#import "CLUPnPDevice.h"
#import "CLUPnPAVPositionInfo.h"
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <net/if.h>
 
#define IOS_CELLULAR    @"pdp_ip0"
#define IOS_WIFI        @"en0"
#define IOS_VPN         @"utun0"
#define IP_ADDR_IPv4    @"ipv4"
#define IP_ADDR_IPv6    @"ipv6"

@interface LyzmlDLNAModule ()<CLUPnPServerDelegate,CLUPnPResponseDelegate>{
    CLUPnPServer *upd;
    CLUPnPRenderer *render;
}
@property (nonatomic, strong) CLUPnPDevice *connectedDevice;
@end

@implementation LyzmlDLNAModule;

//@synthesize weexInstance;

UNI_EXPORT_METHOD(@selector(startSearch:));
UNI_EXPORT_METHOD(@selector(playVideo:callback:));
UNI_EXPORT_METHOD(@selector(pauseVideo));
UNI_EXPORT_METHOD(@selector(resumeVideo:));
UNI_EXPORT_METHOD(@selector(stopVideo));
UNI_EXPORT_METHOD(@selector(setPlayMonitor:));
UNI_EXPORT_METHOD(@selector(getSearchResult:));
UNI_EXPORT_METHOD(@selector(getIpAddress:));
UNI_EXPORT_METHOD(@selector(getVolume));
//UNI_EXPORT_METHOD(@selector(getMinVolumeValue));
//UNI_EXPORT_METHOD(@selector(getMaxVolumeValue));
UNI_EXPORT_METHOD(@selector(setVolume:));
//UNI_EXPORT_METHOD(@selector(setMute:));
UNI_EXPORT_METHOD(@selector(getPositionInfo));
UNI_EXPORT_METHOD(@selector(getMediaDuration));
UNI_EXPORT_METHOD(@selector(seek:));
UNI_EXPORT_METHOD(@selector(getTransportState));
UNI_EXPORT_METHOD(@selector(stopSearch));

UniModuleKeepAliveCallback searchCallback;
UniModuleKeepAliveCallback monitorCallback;

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"====dealloc=====");
}

- (instancetype)init
{
    if (self = [super init]) {
        NSLog(@"====lyzmlDLNA init=====");
        upd = [CLUPnPServer shareServer];
        upd.delegate = self;
    }
    return self;
}


//开始搜索，通过回调返回搜索结果
- (void)startSearch:(UniModuleKeepAliveCallback)callback
{
    if(callback){
        searchCallback = callback;
        [upd start];
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@0 forKey:@"code"];
        [retObj setObject:@"search started!" forKey:@"desc"];
        searchCallback(retObj,YES);
    }else{
        NSLog(@"====lyzmlDLNA _startSearch cancel,no callback=====");
    }
    
    [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(stopSearch) userInfo:nil repeats:NO];
}

#pragma mark -
#pragma mark -- 搜索协议CLUPnPDeviceDelegate回调 --
- (void)upnpSearchChangeWithResults:(NSArray<CLUPnPDevice *> *)devices{
    NSMutableArray *dataArray = [NSMutableArray array];
    for(int i=0;i<devices.count;i++){
       CLUPnPDevice *dev = devices[i];
       NSMutableDictionary *devDic = [NSMutableDictionary dictionary];
       [devDic setObject:dev.friendlyName forKey:@"name"];
       [devDic setObject:[dev.location host] forKey:@"ip"];
       [devDic setObject:dev.uuid forKey:@"udn"];
       [dataArray addObject:devDic];
    }
    
    if(searchCallback && dataArray.count>0){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@1 forKey:@"code"];
        [retObj setObject:@"search result change!" forKey:@"desc"];
        [retObj setObject:dataArray forKey:@"result"];
        searchCallback(retObj,YES);
    }
}

- (void)upnpSearchErrorWithError:(NSError *)error{
    NSLog(@"====dlna search error==%@", error);
    if(searchCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@3 forKey:@"code"];
        [retObj setObject:@"search error" forKey:@"desc"];
        [retObj setObject:error forKey:@"result"];
        searchCallback(retObj,YES);
    }
}

//停止搜索
- (void)stopSearch
{
    [upd stop];
    if(searchCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@2 forKey:@"code"];
        [retObj setObject:@"search finished!" forKey:@"desc"];
        searchCallback(retObj,YES);
    }
    
}

//通过IP来连接设备，同时播放视频
- (void)playVideo:(NSMutableDictionary *)params callback:(UniModuleKeepAliveCallback)callback
{
    NSMutableDictionary *retDic = [NSMutableDictionary dictionary];
//    NSString *ip = [params objectForKey:@"ip"];
    NSString *udn = [params objectForKey:@"udn"];
    NSString *videoUrl = [params objectForKey:@"mediaURL"];
    
    if(udn != nil && videoUrl != nil){
        if(self.connectedDevice!=nil && [udn isEqualToString:self.connectedDevice.uuid]){
            //已连接
            [render setAVTransportURL:videoUrl];
        }else{
            //未连接或更换设备
            [self connectDevice:udn];
            [render setAVTransportURL:videoUrl];
        }
        [retDic setObject:@0 forKey:@"code"];
        [retDic setObject:@"播放成功" forKey:@"desc"];
    }else{
        [retDic setObject:@1 forKey:@"code"];
        [retDic setObject:@"参数错误" forKey:@"desc"];
    }
    if(callback){
        callback(retDic,YES);
    }
}

//暂停播放
- (void)pauseVideo
{
    if(render){
        [render pause];
    }
}

//继续播放,second随便传
- (void)resumeVideo:(NSInteger *)second
{
    if(render){
        [render play];
    }
}

//停止播放
- (void)stopVideo
{
    if(render){
        [render stop];
    }
}

- (void)setPlayMonitor:(UniModuleKeepAliveCallback)callback
{
    if(callback){
        monitorCallback = callback;
    }
}

//主动获取搜索结果
- (void)getSearchResult:(UniModuleKeepAliveCallback)callback
{
    if(callback){
        NSArray<CLUPnPDevice *> *devices = [upd getDeviceList];
        NSMutableArray *dataArray = [NSMutableArray array];
        for(int i=0;i<devices.count;i++){
           CLUPnPDevice *dev = devices[i];
           NSMutableDictionary *devDic = [NSMutableDictionary dictionary];
           [devDic setObject:dev.friendlyName forKey:@"name"];
           [devDic setObject:[dev.location host] forKey:@"ip"];
           [devDic setObject:dev.uuid forKey:@"udn"];
           [dataArray addObject:devDic];
        }
        callback(dataArray,YES);
    }
}

//获取设备当前网络IP地址
- (void)getIpAddress:(UniModuleKeepAliveCallback)callback
{
    BOOL preferIPv4 = true;
    NSArray *searchArray = preferIPv4 ?
    @[ IOS_VPN @"/" IP_ADDR_IPv4, IOS_WIFI @"/" IP_ADDR_IPv4,IOS_CELLULAR @"/" IP_ADDR_IPv4] :
    @[ IOS_VPN @"/" IP_ADDR_IPv6, IOS_WIFI @"/" IP_ADDR_IPv6,IOS_CELLULAR @"/" IP_ADDR_IPv6] ;
    
    NSDictionary *addresses = [self getIpAddressAll];
    NSLog(@"addresses: %@", addresses);
    
    __block NSString *address;
    [searchArray enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop)
     {
         address = addresses[key];
         NSLog(@"key==%@,address==%@",key,address);
         if(address) *stop = YES;
     } ];
    address = address ? address : @"0.0.0.0";
    if(callback){
        callback(address,YES);
    }
}

- (void)getVolume
{
    if(render){
        [render getVolume];
    }
}

- (void)setVolume:(int)volume
{
    if(render){
        NSString *volumeStr = [NSString stringWithFormat: @"%d", volume];
        NSLog(@"setVolume==%@",volumeStr);
        [render setVolumeWith:volumeStr];
        [render getVolume];
    }
}

- (void)getPositionInfo
{
    if(render){
        [render getPositionInfo];
    }
}

- (void)getMediaDuration
{
    if(render){
        [render getMediaDuration];
    }
}

- (void)seek:(float)second
{
    if(render){
        NSLog(@"seek==%f",second);
        [render seek:second];
    }
}

- (void)getTransportState
{
    if(render){
        [render getTransportInfo];
    }
}

/**
 * 连接设备，初始化控制器render
 */
- (void)connectDevice:(NSString *)udn
{
    NSArray<CLUPnPDevice *> *deviceList = [upd getDeviceList];
    if(deviceList != nil && deviceList.count>0){
        for(int i=0;i<deviceList.count;i++){
            CLUPnPDevice *dev = deviceList[i];
            NSString *devUuid = dev.uuid;
            if([udn isEqualToString:devUuid]){
                self.connectedDevice = dev;
                render = [[CLUPnPRenderer alloc] initWithModel:self.connectedDevice];
                render.delegate = self;
            }
        }
    }else{
        self.connectedDevice = nil;
        render = nil;
    }
}



#pragma mark -
#pragma mark - 控制协议CLUPnPResponseDelegate回调-
- (void)upnpSetAVTransportURIResponse{
    NSLog(@"====upnpSetAVTransportURIResponse====");
    [render play];
}

- (void)upnpGetTransportInfoResponse:(CLUPnPTransportInfo *)info{
    NSLog(@"====upnpSetAVTransportURIResponse====%@，%@，%@",info.currentTransportState,info.currentTransportStatus,info.currentSpeed);
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@1 forKey:@"code"];
        [retObj setObject:@"onGetTransportState" forKey:@"desc"];
        [retObj setObject:info.currentTransportState forKey:@"result"];
        monitorCallback(retObj,YES);
    }
}

- (void)upnpGetMediaDurationResponse:(CLUPnPMediaInfo *)info{
    NSLog(@"====upnpGetMediaDurationResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@2 forKey:@"code"];
        [retObj setObject:@"onGetMediaDuration" forKey:@"desc"];
        NSString *mediaDurationStr = [NSString stringWithFormat:@"%f",info.mediaDuration];
        
        NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
        if([format numberFromString:mediaDurationStr]){
            id durationFloat = [NSNumber numberWithFloat:[mediaDurationStr floatValue]];
            [retObj setObject:durationFloat forKey:@"result"];
        }else{
            [retObj setObject:mediaDurationStr forKey:@"result"];
        }
        monitorCallback(retObj,YES);
    }
}

- (void)upnpPlayResponse{
    NSLog(@"====upnpPlayResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@3 forKey:@"code"];
        [retObj setObject:@"onPlay" forKey:@"desc"];
        monitorCallback(retObj,YES);
    }
}

- (void)upnpPauseResponse{
    NSLog(@"====upnpPauseResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@4 forKey:@"code"];
        [retObj setObject:@"onPause" forKey:@"desc"];
        monitorCallback(retObj,YES);
    }
}

- (void)upnpStopResponse{
    NSLog(@"====upnpStopResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@5 forKey:@"code"];
        [retObj setObject:@"onStop" forKey:@"desc"];
        monitorCallback(retObj,YES);
    }
}

- (void)upnpSetVolumeResponse{
    NSLog(@"====upnpSetVolumeResponse====");
    if(monitorCallback){
        [render getVolume];
    }
}

- (void)upnpGetVolumeResponse:(NSString *)volume{
    NSLog(@"====upnpGetVolumeResponse====%@",volume);
    if(monitorCallback && volume != nil && volume.length>0){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@6 forKey:@"code"];
        [retObj setObject:@"onVolumeChanged" forKey:@"desc"];
        
        NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
        if([format numberFromString:volume]){
            id volumeInt = [NSNumber numberWithInteger:[volume integerValue]];
            [retObj setObject:volumeInt forKey:@"result"];
        }
        
        monitorCallback(retObj,YES);
    }
}

- (void)upnpGetPositionInfoResponse:(CLUPnPAVPositionInfo *)info{
    NSLog(@"====upnpGetPositionInfoResponse====%f,%f,%f",info.absTime,info.relTime,info.trackDuration);
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@7 forKey:@"code"];
        [retObj setObject:@"onProgressUpdated" forKey:@"desc"];
        NSString *absTimeStr = [NSString stringWithFormat:@"%f",info.absTime];
        
        NSNumberFormatter *format = [[NSNumberFormatter alloc]init];
        if([format numberFromString:absTimeStr]){
            id timeFloat = [NSNumber numberWithFloat:[absTimeStr floatValue]];
            [retObj setObject:timeFloat forKey:@"result"];
        }else{
            [retObj setObject:absTimeStr forKey:@"result"];
        }
        monitorCallback(retObj,YES);
    }
}

- (void)upnpSeekResponse{
    NSLog(@"====upnpSeekResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@8 forKey:@"code"];
        [retObj setObject:@"onSeekComplete" forKey:@"desc"];
        monitorCallback(retObj,YES);
    }
}

- (void)upnpUndefinedResponse{
    NSLog(@"====upnpSeekResponse====");
    if(monitorCallback){
        NSMutableDictionary *retObj = [NSMutableDictionary dictionary];
        [retObj setObject:@9 forKey:@"code"];
        [retObj setObject:@"onError" forKey:@"desc"];
        monitorCallback(retObj,YES);
    }
}


- (NSDictionary *)getIpAddressAll
{
    NSMutableDictionary *addresses = [NSMutableDictionary dictionaryWithCapacity:8];
     
    // retrieve the current interfaces - returns 0 on success
    struct ifaddrs *interfaces;
    if(!getifaddrs(&interfaces)) {
        // Loop through linked list of interfaces
        struct ifaddrs *interface;
        for(interface=interfaces; interface; interface=interface->ifa_next) {
            if(!(interface->ifa_flags & IFF_UP) /* || (interface->ifa_flags & IFF_LOOPBACK) */ ) {
                continue; // deeply nested code harder to read
            }
            const struct sockaddr_in *addr = (const struct sockaddr_in*)interface->ifa_addr;
            char addrBuf[ MAX(INET_ADDRSTRLEN, INET6_ADDRSTRLEN) ];
            if(addr && (addr->sin_family==AF_INET || addr->sin_family==AF_INET6)) {
                NSString *name = [NSString stringWithUTF8String:interface->ifa_name];
                NSString *type;
                if(addr->sin_family == AF_INET) {
                    if(inet_ntop(AF_INET, &addr->sin_addr, addrBuf, INET_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv4;
                    }
                } else {
                    const struct sockaddr_in6 *addr6 = (const struct sockaddr_in6*)interface->ifa_addr;
                    if(inet_ntop(AF_INET6, &addr6->sin6_addr, addrBuf, INET6_ADDRSTRLEN)) {
                        type = IP_ADDR_IPv6;
                    }
                }
                if(type) {
                    NSString *key = [NSString stringWithFormat:@"%@/%@", name, type];
                    addresses[key] = [NSString stringWithUTF8String:addrBuf];
                }
            }
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return [addresses count] ? addresses : nil;
}


@end
