//
//  LyzmlDLNAModule.h
//  libWeexLyzmlDLNA
//
//  Created by edz on 2020/3/31.
//  Copyright © 2020 coolanimals. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "WXModuleProtocol.h"
#import "DCUniModule.h"

NS_ASSUME_NONNULL_BEGIN

@interface LyzmlDLNAModule : DCUniModule

@end

NS_ASSUME_NONNULL_END
