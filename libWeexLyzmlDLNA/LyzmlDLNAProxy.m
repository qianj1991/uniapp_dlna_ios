//
//  LyzmlDLNAProxy.m
//  libWeexLyzmlDLNA
//
//  Created by edz on 2020/3/31.
//  Copyright © 2020 coolanimals. All rights reserved.
//

#import "LyzmlDLNAProxy.h"

@implementation LyzmlDLNAProxy
-(void)onCreateUniPlugin{
    NSLog(@"有需要初始化的逻辑可以放这里！");
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    NSLog(@"有需要didFinishLaunchingWithOptions可以放这里！");
    return YES;
}

@end

